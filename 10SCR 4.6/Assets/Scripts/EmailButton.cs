﻿using UnityEngine;
using System.Collections;

public class EmailButton : MonoBehaviour
{
	private float height = (Screen.height / 5);
	private float width = (Screen.width / 11);
	private float xPos = (0);
	private float yPos = (0);
	public static float BUFFER = (Screen.width / 55);
	public bool button_on;
	public int email_count;
	public ArrayList email_list;
	public static ArrayList removed_components;

	public void CreateButton(string topic, Texture icon, ArrayList emails)
	{
		if (removed_components == null)
		{
			removed_components = new ArrayList();
		}
		email_list = emails;
		email_count = email_list.Count;
		xPos = (Screen.width - width);
		button_on = true;
	}

	public void OnGUI()
	{
		if (button_on)
		{
			if (GUI.Button (new Rect (xPos, yPos, width, height), "icon"))
			{
				//open email.
				removed_components.Add(email_count);
				email_list.RemoveAt(email_list.Count - 1);
				email_count = -1;
				button_on = false;
			}
		}
	}

	public void Update()
	{
		for (int i = 0; i < removed_components.Count; i++)
		{
			if (email_count == (int)removed_components[i])
			{
				removed_components.RemoveAt(i);
			}
			else if (email_count > (int)removed_components[i])
			{
				email_count --;
			}
		}
		if (xPos >= ((Screen.width / 11 * email_count) - width + (BUFFER * email_count)))		
		{
			xPos = xPos - 3.5f;
		}	
	}
}
