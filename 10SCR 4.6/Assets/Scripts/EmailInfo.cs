﻿using UnityEngine;
using System.Collections.Generic;

public class EmailInfo
{
	private IList<string> email_topics;
	private IList<string> email_icons;
	private GameObject[] email_icon_objects;

	public IList<string> Email_Topics
	{
		get { return email_topics; }
	}
	
	public IList<string> Email_Icons
	{
		get { return email_icons; }
	}

	public void OnStart()
	{
		email_icons = new List<string>();
		email_topics = new List<string>();
		email_icon_objects = GameObject.FindGameObjectsWithTag("email_icon");
		for (int i = 0; i < email_icon_objects.Length; i++)
		{
			email_icons.Add (email_icon_objects[i].GetComponent<GUITexture>().texture.name);
		}
		
		email_topics.Add ("test");
	}
	
	public EmailInfo()
	{
/*		email_icons = new List<string>();
		email_icon_objects = GameObject.FindGameObjectsWithTag("email_icon");
		for (int i = 0; i < email_icon_objects.Length; i++)
		{
			email_icons.Add (email_icon_objects[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		email_topics.Add ("test"); */
	}
}
