﻿using UnityEngine;
using System.Collections;

public class CreateEmail
{
	public static int EMAIL_ARR_LEN = 10;

	public class Email
	{
		public string topic;
		public Texture icon;
	}	

	private RandomizeElements rnd;
	private EmailInfo info;
	private GameObject button;
	private static ArrayList emails;
	private string topic;
	private Texture icon;
	
	public void Make_Email()
	{
		rnd = new RandomizeElements();
		info = new EmailInfo();
		info.OnStart ();

		button = new GameObject ();
		button.AddComponent<EmailButton>();
		if (emails == null)
		{
			emails = new ArrayList ();
		}
		int rndNum = rnd.getRandom (info.Email_Topics);
		topic = info.Email_Topics[rndNum];
		icon = GameObject.Find(info.Email_Icons[rnd.getRandom(info.Email_Icons)]).GetComponent<GUITexture>().texture;
		Email email = new Email();
		email.topic = topic;
		email.icon = icon;
		emails.Add(email);
		
		if (emails.Count < EMAIL_ARR_LEN)
		{
			EmailButton b = button.GetComponent<EmailButton>();
			b.CreateButton(topic, icon, emails);
		}
		else
		{
			/* Button squish function */
		}
	}
	
	public void Make_Set_Email()
	{
		// NOT IMPLIMENTED YET
	}
}
