﻿using UnityEngine;
using System.Collections.Generic;

public class RandomizeElements
{
    private ItemLists lists;
	private string imageOne;
	private string imageTwo;
	private string imageThree;
    
	public int getRandom(IList<string> list)
	{
		System.Random rnd = new System.Random ();
		int rndNumber = rnd.Next (0, list.Count);
		return rndNumber;
	}

/*	NO LONGER RELEVANT - CHANGE IN GDD.
	public bool isGuilty()
	{
		System.Random rnd = new System.Random ();
		int rndNumber = rnd.Next (0, 2);
		if (rndNumber == 0)
		{
			return false;
		}
		else
			return true;
	}	*/
	
	public string ReturnItem(IList<string> itemList)
	{
		string image;
//	    GetInnocentList gil = new GetInnocentList();
//		currentList = gil.getInnocentList(currentList, itemList, num);	NO LONGER NEEDED
		image = itemList[getRandom(itemList)];
		return image;	
	}

	public void getItems()
	{
		lists = new ItemLists();
//	    bool guilty = isGuilty();	NO LONGER RELEVANT
//		IList<string> currentList = lists.Test_List;
		IList<string> itemList;
	
		itemList = lists.Item_List_One;
		do
		{
			imageOne = ReturnItem (itemList);
		}
		while (imageOne == GameObject.FindGameObjectWithTag("item_one").GetComponent<SpriteRenderer>().sprite.name);
		GameObject.FindGameObjectWithTag("item_one").GetComponent<SpriteRenderer>().sprite 
		= GameObject.Find(imageOne).GetComponent<SpriteRenderer>().sprite;
		
		itemList = lists.Item_List_Two;
		do
		{
			imageTwo = ReturnItem (itemList);
		}
		while (imageTwo == GameObject.FindGameObjectWithTag("item_two").GetComponent<SpriteRenderer>().sprite.name);
		GameObject.FindGameObjectWithTag("item_two").GetComponent<SpriteRenderer>().sprite 
		= GameObject.Find(imageTwo).GetComponent<SpriteRenderer>().sprite;
		
		itemList = lists.Item_List_Three;
		do
		{
			imageThree = ReturnItem (itemList);
		}
		while (imageThree == GameObject.FindGameObjectWithTag("item_three").GetComponent<SpriteRenderer>().sprite.name);
		GameObject.FindGameObjectWithTag("item_three").GetComponent<SpriteRenderer>().sprite 
		= GameObject.Find(imageThree).GetComponent<SpriteRenderer>().sprite;
	}
}
