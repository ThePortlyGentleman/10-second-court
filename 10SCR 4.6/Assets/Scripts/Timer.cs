﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour
{
	private bool clock_run;
	private float time_left;
	private float last_time;
	private float next_email;
	public static float TIME_CHUNK = 0.05f;
	public static float MIN_EMAILS = 10f;
	public static float MAX_EMAILS = 20f;
	CreateEmail email = new CreateEmail();

	public void Reset_Timer ()
	{
		time_left = 10f;
		clock_run = true;
		last_time = Time.time;
	}
		
	void Update ()
	{
		if (clock_run == true && Time.time >= last_time + TIME_CHUNK && time_left > 0f)
		{
			if (next_email <= Time.time)
			{
				email.Make_Email();
				next_email = Time.time + Random.Range (MIN_EMAILS, MAX_EMAILS);
			}
			time_left -= Time.time - last_time;
			last_time = Time.time;
			GameObject.Find("GameClock").GetComponent<GUIText>().text = time_left.ToString("F1");
		}
		else if (time_left <= 0)
		{
			clock_run = false;
			time_left = 10f;
			Application.LoadLevel("Game_Over");
		}
	}

	void Start()
	{
		GameObject.Find("GameClock").GetComponent<Timer>().Reset_Timer();
		next_email = Time.time + Random.Range (MIN_EMAILS, MAX_EMAILS);
	}
}
