﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour
{
	RandomizeElements rnd = new RandomizeElements();
	CreateProfile prof = new CreateProfile();
	
	public void ButtonCall ()
	{
		rnd.getItems();
		prof.Create_Profile();
		GameObject.Find("GameClock").GetComponent<Timer>().Reset_Timer();
	}
}
