﻿using UnityEngine;
using System.Collections.Generic;

public class ProfileLists
{
	private IList<string> body;
	private GameObject[] body_items;
	private IList<string> skin_col;
	private GameObject[] skin_col_items;
	private IList<string> hair;
	private GameObject[] hair_items;
	private IList<string> eye_shape;
	private GameObject[] eye_shape_items;
	private IList<string> eye_col;
	private GameObject[] eye_col_items;
	private IList<string> eyebrow_col;
	private GameObject[] eyebrow_col_items;
	private IList<string> mouth;
	private GameObject[] mouth_items;
	private IList<string> mouth_col;
	private GameObject[] mouth_col_items;
	private IList<string> nose;
	private GameObject[] nose_items;
	
	public IList<string> Body
	{
		get { return body; }
	}
	public IList<string> Skin_Col
	{
		get { return skin_col; }
	}
	public IList<string> Hair
	{
		get { return hair; }
	}
	public IList<string> Eye_Shape
	{
		get { return eye_shape; }
	}
	public IList<string> Eye_Col
	{
		get { return eye_col; }
	}
	public IList<string> Eyebrow_Col
	{
		get { return eyebrow_col; }
	}
	public IList<string> Mouth
	{
		get { return mouth; }
	}
	public IList<string> Mouth_Col
	{
		get { return mouth_col; }
	}
	public IList<string> Nose
	{
		get { return nose; }
	}

	public ProfileLists()
	{
		body = new List<string>();
		body_items = GameObject.FindGameObjectsWithTag("list_body");
		for (int i = 0; i < body_items.Length; i++)
		{
			body.Add (body_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		skin_col = new List<string>();
		skin_col_items = GameObject.FindGameObjectsWithTag("list_skin_col");
		for (int i = 0; i < skin_col_items.Length; i++)
		{
			skin_col.Add (skin_col_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		hair = new List<string>();
		hair_items = GameObject.FindGameObjectsWithTag("list_hair");
		for (int i = 0; i < hair_items.Length; i++)
		{
			hair.Add (hair_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		eye_shape = new List<string>();
		eye_shape_items = GameObject.FindGameObjectsWithTag("list_eye_shape");
		for (int i = 0; i < eye_shape_items.Length; i++)
		{
			eye_shape.Add (eye_shape_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		eye_col = new List<string>();
		eye_col_items = GameObject.FindGameObjectsWithTag("list_eye_col");
		for (int i = 0; i < eye_col_items.Length; i++)
		{
			eye_col.Add (eye_col_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		eyebrow_col = new List<string>();
		eyebrow_col_items = GameObject.FindGameObjectsWithTag("list_eyebrow_col");
		for (int i = 0; i < eyebrow_col_items.Length; i++)
		{
			eyebrow_col.Add (eyebrow_col_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		mouth = new List<string>();
		mouth_items = GameObject.FindGameObjectsWithTag("list_mouth");
		for (int i = 0; i < mouth_items.Length; i++)
		{
			mouth.Add (mouth_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		mouth_col = new List<string>();
		mouth_col_items = GameObject.FindGameObjectsWithTag("list_mouth_col");
		for (int i = 0; i < mouth_col_items.Length; i++)
		{
			mouth_col.Add (mouth_col_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}

		nose = new List<string>();
		nose_items = GameObject.FindGameObjectsWithTag("list_nose");
		for (int i = 0; i < nose_items.Length; i++)
		{
			nose.Add (nose_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}
	}
}
