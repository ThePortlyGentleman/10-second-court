﻿using UnityEngine;
using System.Collections.Generic;

public class CreateProfile
{ 
	private ProfileLists p_list;
	private RandomizeElements rnd;
	private System.Random sys_rnd;
	private string gender;
	private string body;
	private string skin_col;
	private string hair;
	private string eye_shape;
	private string eye_col;
	private string eyebrow_col;
	private string mouth;
	private string mouth_col;
	private string nose;

	public void Create_Profile()
	{
		p_list = new ProfileLists ();
		rnd = new RandomizeElements();
		sys_rnd = new System.Random();
		// Get gender.
		if (sys_rnd.Next (0, 2) == 0)
		{
			gender = "Male";
		}
		else
		{
			gender = "Female";
		}
		gender = "Male";
Debug.Log ("gender: " + gender);
		Debug.Log ("p_list.Body: " + p_list.Body);
		// Get body.
		do
		{
			body = rnd.ReturnItem (p_list.Body);
			Debug.Log("body: " + body);
			Debug.Log("body.StartsWith(gender): " + body.StartsWith(gender));
		}
		while (body == GameObject.FindGameObjectWithTag("body").GetComponent<SpriteRenderer>().sprite.name
		       || body.StartsWith(gender) == false);
		Debug.Log ("-----");
		GameObject.FindGameObjectWithTag("body").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(body).GetComponent<SpriteRenderer>().sprite;
Debug.Log("body: " + body);

		// Get skin colour
		do
		{
			skin_col = rnd.ReturnItem (p_list.Skin_Col);
		}
		while (skin_col == GameObject.FindGameObjectWithTag("skin_col").GetComponent<SpriteRenderer>().sprite.name
		       || skin_col.StartsWith(body) == false);
		GameObject.FindGameObjectWithTag("skin_col").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(body + " " + skin_col).GetComponent<SpriteRenderer>().sprite;
Debug.Log("skin_col: " + skin_col);

		// Get hair.
		do
		{
			hair = rnd.ReturnItem (p_list.Hair);
		}
		while (hair == GameObject.FindGameObjectWithTag("hair").GetComponent<SpriteRenderer>().sprite.name
		       || hair.StartsWith(body) == false);
		GameObject.FindGameObjectWithTag("hair").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(body + " " + hair).GetComponent<SpriteRenderer>().sprite;
Debug.Log("hair: " + hair);

		// Get eye shape.
		do
		{
			eye_shape = rnd.ReturnItem (p_list.Eye_Shape);
		}
		while (eye_shape == GameObject.FindGameObjectWithTag("eye_shape").GetComponent<SpriteRenderer>().sprite.name);
		GameObject.FindGameObjectWithTag("eye_shape").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(gender + "" + eye_shape).GetComponent<SpriteRenderer>().sprite;
Debug.Log("eye_shape: " + eye_shape);

		// Get eye colour.
		do
		{
			eye_col = rnd.ReturnItem (p_list.Eye_Col);
		}
		while (eye_col == GameObject.FindGameObjectWithTag("eye_col").GetComponent<SpriteRenderer>().sprite.name
		       || eye_col.StartsWith(eye_shape) == false);
		GameObject.FindGameObjectWithTag("eye_col").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(eye_shape + " " + eye_col).GetComponent<SpriteRenderer>().sprite;
Debug.Log("eye_col: " + eye_col);

		// Get eyebrow colour.
		do
		{
			eyebrow_col = rnd.ReturnItem (p_list.Eyebrow_Col);
		}
		while (eyebrow_col == GameObject.FindGameObjectWithTag("eyebrow_col").GetComponent<SpriteRenderer>().sprite.name
		       || eyebrow_col.StartsWith(eye_shape) == false);
		GameObject.FindGameObjectWithTag("eyebrow_col").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(eye_shape + " " + eyebrow_col).GetComponent<SpriteRenderer>().sprite;
Debug.Log("eyebrow_col: " + eyebrow_col);

		// Get mouth.
		do
		{
			mouth = rnd.ReturnItem (p_list.Mouth);
		}
		while (mouth == GameObject.FindGameObjectWithTag("mouth").GetComponent<SpriteRenderer>().sprite.name
		       || mouth.StartsWith(gender) == false);
		GameObject.FindGameObjectWithTag("mouth").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(gender + " " + mouth).GetComponent<SpriteRenderer>().sprite;
Debug.Log("mouth: " + mouth);

		// Get mouth colour.
		do
		{
			mouth_col = rnd.ReturnItem (p_list.Mouth_Col);
		}
		while (mouth_col == GameObject.FindGameObjectWithTag("mouth_col").GetComponent<SpriteRenderer>().sprite.name
		       || mouth_col.StartsWith(mouth) == false);
		GameObject.FindGameObjectWithTag("mouth_col").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(mouth + " " + mouth_col).GetComponent<SpriteRenderer>().sprite;
Debug.Log("mouth_col: " + mouth_col);

		// Get nose.
		do
		{
			nose = rnd.ReturnItem (p_list.Nose);
		}
		while (nose == GameObject.FindGameObjectWithTag("nose").GetComponent<SpriteRenderer>().sprite.name
		       || nose.StartsWith(gender) == false);
		GameObject.FindGameObjectWithTag("nose").GetComponent<SpriteRenderer>().sprite 
			= GameObject.Find(gender + " " + nose).GetComponent<SpriteRenderer>().sprite;
Debug.Log("nose: " + nose);
	}
}
