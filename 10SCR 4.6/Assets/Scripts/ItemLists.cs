﻿using UnityEngine;
using System.Collections.Generic;

public class ItemLists
{
	private IList<string> item_list_one;
	private GameObject[] item_list_one_items;
	private IList<string> item_list_two;
	private GameObject[] item_list_two_items;
	private IList<string> item_list_three;
	private GameObject[] item_list_three_items;

//	private IList<string> test_list;
		
	public IList<string> Item_List_One
	{
		get { return item_list_one; }
	} 	
	
	public IList<string> Item_List_Two 
	{
		get { return item_list_two; }
	}
	
	public IList<string> Item_List_Three 
	{
		get { return item_list_three; }
	}										
	
/*	public IList<string> Test_List	NO LONGER RELEVANT.
	{
		get { return test_list; }
	}	*/
	
    public ItemLists()
    {    
		item_list_one = new List<string>();
		item_list_one_items = GameObject.FindGameObjectsWithTag("item_list_one");
		for (int i = 0; i < item_list_one_items.Length; i++)
		{
			item_list_one.Add (item_list_one_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}
		
		item_list_two = new List<string>();
		item_list_two_items = GameObject.FindGameObjectsWithTag("item_list_two");
		for (int i = 0; i < item_list_two_items.Length; i++)
		{
			item_list_two.Add (item_list_two_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}
		
		item_list_three = new List<string>();
		item_list_three_items = GameObject.FindGameObjectsWithTag("item_list_three");
		for (int i = 0; i < item_list_three_items.Length; i++)
		{
			item_list_three.Add (item_list_three_items[i].GetComponent<SpriteRenderer>().sprite.name);
		}
/*  NO LONGER RELIVANT - CHANGE IN GDD.		
		test_list = new List<string>();
		test_list.Add ("TestImages_0");
		test_list.Add ("TestImages_7");
		test_list.Add ("TestImages_11");	*/
    }																																																																																																											
}
